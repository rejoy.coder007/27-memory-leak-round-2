package com.example.rambo.zk_leakythread;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.rambo.zk_leakythread.aa_home_page.aa_activity_home_screen;
import com.example.rambo.zk_leakythread.za_global.MyApplication;

import java.util.List;

public class aa_activity_StartUp extends AppCompatActivity
{

    private FirstTimeThread                                 firstTimeThread = new FirstTimeThread();
    private SplashScreenThread                        splashScreenThread = new SplashScreenThread();
    public  static aa_activity_StartUp                                               aaStartUp=null;
    ViewGroup                                                                           layout=null;
    protected static ActivityManager                                           activityManager=null;
    static String                                   name_file=  "com.example.rambo.zk_leakythread.";

public static Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        aa_activity_StartUp.aaStartUp = this;


        if(zz_Config.TEST){
            Log.d("_#_ON _CREATE", "onCreate: aa_activity_StartUp -- A");
        }


        if(activityManager==null)
        {
            activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        }
        if(isFirstTime())
        {
            setContentView(R.layout.aa_activity_startup);
            FirstTimeScreen();
        }
        else
        {
            setContentView(R.layout.ab_activity_splash_up);
         SplashTimeScreen();
           // FirstTimeScreen();
        }

        Runtime.getRuntime().gc();

        if(zz_Config.TEST){
            Log.d("_#_ON_CREATE", "onCreate: aa_activity_StartUp -- B");
        }

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if(zz_Config.TEST){
            Log.d("_#_ON_RESUME", "onResume: aa_activity_StartUp -- A");
        }

        if(zz_Config.IS_BACK){



            Runtime.getRuntime().gc();
             if(zz_Config.TEST)
             {
                 Log.d("_#APP_STATE_S",getAppTaskState());
             }
           finishAndRemoveTask();
        }


        if(zz_Config.TEST){
            Log.d("_#_ON_RESUME", "onResume: aa_activity_StartUp -- B");
        }


    }

    private boolean isFirstTime()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_StartUp " + ranBefore);

        return !ranBefore;
    }


    private void FirstTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- A ");

        firstTimeThread.start();

        if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_SCREEN", "FirstTimeScreen: aa_activity_StartUp -- B ");

    }

    private void SplashTimeScreen()
    {
        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- A ");

        splashScreenThread.start();

        if(zz_Config.TEST)
            Log.d("_#SPLASH_SCREEN", "SplashTimeScreen: aa_activity_StartUp -- B ");
    }

    private static void redirectToNewScreen_start()
    {

        if(zz_Config.TEST)
            Log.d("_#REDIR_START_SCREEN", "redirectToNewScreen_start() A : aa_activity_StartUp");




        Intent intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);
        aa_activity_StartUp.aaStartUp.startActivity(intent);


        if(zz_Config.TEST)
            Log.d("_#REDIR_START_SCREEN_S", "redirectToNewScreen_start()B : aa_activity_StartUp");




    }

    private static void redirectToNewScreen_splash()
    {

        if(zz_Config.TEST)
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() A : aa_activity_StartUp");


        Intent intent = new Intent(aa_activity_StartUp.aaStartUp, aa_activity_home_screen.class);
        aa_activity_StartUp.aaStartUp.startActivity(intent);


        if(zz_Config.TEST)
            Log.d("_#REDIR_SPLASH_SCREEN", "redirectToNewScreen_splash() B : aa_activity_StartUp");


    }


    @Override
    protected void onPause() {
        super.onPause();

        if(zz_Config.TEST)
            Log.d("_#_ON_PAUSE", "onPause  :  aa_activity_StartUp A");

       // firstTimeThread.interrupt();
     //   splashScreenThread.interrupt();
        //firstTimeThread=null;
        //splashScreenThread=null;

/*
        if(isFirstTime())
        {
            layout = (ConstraintLayout) findViewById(R.id.startup_id);
           // layout.removeAllViews();
        }
        else
        {
            layout = (ConstraintLayout) findViewById(R.id.splash_id);
           // layout.removeAllViews();
        }

*/

        layout=null;
       // aa_activity_StartUp.aaStartUp = null;

        Runtime.getRuntime().gc();
        if(zz_Config.TEST)
            Log.d("_#_ON_PAUSE", "onPause  :  aa_activity_StartUp B");

    }

    @Override
    public void onDestroy()
    {

        if(zz_Config.TEST)
            Log.d("_#_DESTROY", "Destroy : aa_activity_StartUp A");

        finishAffinity();
        super.onDestroy();


        if(zz_Config.TEST)
            Log.d("_#_DESTROY", "Destroy: aa_activity_StartUp B");



    }


    private static class FirstTimeThread extends Thread {
        @Override
        public void run() {

            if(zz_Config.TEST)
                Log.d("_#FIRST_TIME_THREAD", " Inside FirstTimeThread Thread :  aa_activity_StartUp A");



            try {
                Thread.sleep(zz_Config.TIME_DELAY_START_SCREEN);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            redirectToNewScreen_start();

            if(zz_Config.TEST)
            Log.d("_#FIRST_TIME_THREAD", " Inside FirstTimeThread Thread :  aa_activity_StartUp B");


        }
    }


    private static class SplashScreenThread extends Thread {
        @Override
        public void run() {

            if(zz_Config.TEST)
                Log.d("_#SPLASH_THREAD", " Inside SplashScreenThread :   aa_activity_StartUp A");





            MyApplication.getContext().getVideo("aa_activity_StartUp");
            redirectToNewScreen_splash();

            if(zz_Config.TEST)
                Log.d("_#SPLASH_THREAD", " Inside SplashScreenThread :   aa_activity_StartUp B");


        }
    }


    protected static String getAppTaskState(){



        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo){
            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace(name_file,"")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace(name_file,"")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }
}
