package com.example.rambo.zk_leakythread.aa_home_page;

import android.Manifest;
import android.app.ActivityManager;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.MenuCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.rambo.zk_leakythread.R;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.Directory;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.aa_home_screen_PagerAdapter;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.example.rambo.zk_leakythread.za_global.MyApplication;
import com.example.rambo.zk_leakythread.zz_Config;

import java.util.ArrayList;
import java.util.List;

public class aa_activity_home_screen extends AppCompatActivity
{
    static final  int                             MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 20;

    protected static ActivityManager activityManager;
    static String name_file=  "com.example.rambo.zk_leakythread.";
    Runnable runnable =null;
    public  Thread readDirectoryThread=null;
    public ImageButton back;
    MyApplication myApplication;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_activity_home_screen);
        myApplication =(MyApplication) getApplication();
        Window window = getWindow();
        Drawable background = getResources().getDrawable(R.drawable.ab_toolbar_bg);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(android.R.color.transparent));
        window.setNavigationBarColor(getResources().getColor(android.R.color.transparent));
        window.setBackgroundDrawable(background);


        Toolbar  toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

   /*

        if(zz_Config.TEST){
            Log.d("_#_ON _CREATE", "onCreate: aa_activity_home_screen -- A");
        }


        myApplication.viewPager = (ViewPager) findViewById(R.id.viewpager);
        enable_tab();
        getPermission();




        if(activityManager==null){
            activityManager=(ActivityManager)getSystemService(ACTIVITY_SERVICE);
        }
        if(zz_Config.TEST){
            Log.d("_#_ON _CREATE", "onCreate: aa_activity_home_screen -- B");
        }
*/

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        MenuCompat.setGroupDividerEnabled(menu, true);


        MenuInflater infl = getMenuInflater();
        infl.inflate(R.menu.main_menu, menu);


        return true;

    }

    public void getPermission()
    {

        if (ActivityCompat.checkSelfPermission(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
        {

            if(zz_Config.TEST){
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- A");
            }
           // setFirstTime_permission();
         //  StartUpScreenThread_dir_fill(true);

          //  enable_tab();





            if(zz_Config.TEST){
                Log.d("_#GOT_PERMISSION", "gotPermission --getPermission : aa_activity_home_screen -- B");
            }



        }
        else
        {


            if (ActivityCompat.shouldShowRequestPermissionRationale(aa_activity_home_screen.this, Manifest.permission.READ_EXTERNAL_STORAGE))
            {


                permission_snack_message();
                if(zz_Config.TEST){
                    Log.d("_#NO_PERMISSION_PREV", "shouldShowRequestPermissionRationale --getPermission : aa_activity_home_scree");
                }

            }
            else
            {
                if(zz_Config.TEST){
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- A");
                }

                ActivityCompat.requestPermissions(aa_activity_home_screen.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                if(zz_Config.TEST){
                    Log.d("_#NO_PERMISSION_FIRST", "noPermission --getPermission : aa_activity_home_screen -- B");
                }


            }
        }


    }

    @Override
    protected void onResume() {

        if(zz_Config.TEST){
            Log.d("_#_ON_RESUME", "onResume: aa_activity_home_screen -- A");
        }

        super.onResume();


        if(zz_Config.TEST){
            Log.d("_#_ON_RESUME", "onResume: aa_activity_home_screen -- B");
        }


    }

    public void permission_snack_message()
    {



        Snackbar.make(findViewById(android.R.id.content),
                "Needs permission to read  video",
                Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        ActivityCompat.requestPermissions(aa_activity_home_screen.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                        if(zz_Config.TEST){
                            Log.d("_#SNACK_BAR", "permission_snack_message : aa_activity_home_screen");
                        }

                    }
                }).show();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {

        switch (requestCode)
        {


            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:
                {

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //                    grantResult[0] means it will check for the first postion permission which is READ_EXTERNAL_STORAGE
                    //                    grantResult[1] means it will check for the Second postion permission which is CAMERA
                    Toast.makeText(aa_activity_home_screen.this, "Permission Granted", Toast.LENGTH_SHORT).show();

                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- A");
                    }

                    setFirstTime_permission();
                    StartUpScreenThread_dir_fill(true);


                    if(zz_Config.TEST){
                        Log.d("_#PERMISSION_RESULT", "granted onRequestPermissionsResult : aa_activity_home_screen -- B");
                    }


                }
                else
                {


                    permission_snack_message();

                }

            }
        }


    }






    private void setFirstTime_permission()
    {


        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // first time
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("Permission", true);
        editor.commit();
        boolean ranBefore = preferences.getBoolean("Permission", false);

        if(zz_Config.TEST)
            Log.d("_#FIRST_FLAG", "isFirstTime shared pref : aa_activity_home_screen " + ranBefore);




    }

    private void enable_tab()
    {

        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab  : aa_activity_home_screen A" );



        // Get the ViewPager and set it's PagerAdapter so that it can display items

        myApplication.viewPager.setAdapter(new aa_home_screen_PagerAdapter(getSupportFragmentManager(),
                aa_activity_home_screen.this));
        // Give the TabLayout the ViewPager
        ((MyApplication) getApplication()).tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ((MyApplication) getApplication()).tabLayout.setupWithViewPager(myApplication.viewPager);

        myApplication.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
               switch (position){
                   case 2 : {
                       back.setVisibility(View.VISIBLE);
                       break;
                   }

                   default:{
                       back.setVisibility(View.GONE);
                   }
               }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        ViewGroup slidingTabStrip =  (ViewGroup)  ((MyApplication) getApplication()).tabLayout.getChildAt(0);
        for (int i = 0; i <  ((MyApplication) getApplication()).tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==2)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }


        myApplication.viewPager.setCurrentItem(1);
      //  viewPager.setCurrentItem(1);
        if(zz_Config.TEST)
            Log.d("_#ENABLE_TAB", "enable_tab : aa_activity_home_screen B" );



    }

    

    public void onClick(View view){


        if(zz_Config.TEST)
            Log.d("_##BACK", "enable_tab : aa_activity_home_screen " );


        ViewGroup slidingTabStrip =  (ViewGroup)  ((MyApplication) getApplication()).tabLayout.getChildAt(0);
        for (int i = 0; i <  ((MyApplication) getApplication()).tabLayout.getTabCount(); i++) {
            View tab = slidingTabStrip.getChildAt(i);
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) tab.getLayoutParams();
            if(i==2)
                layoutParams.weight = 0;
            else
                layoutParams.weight = 1;
            tab.setLayoutParams(layoutParams);


        }

    }
/*
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {






        switch (event.getKeyCode())
        {
            case KeyEvent.KEYCODE_A:
            {
                //your Action code
                if(zz_Config.TEST)
                {
                    Log.d("_#TASK_H",getAppTaskState());
                }


                return true;
            }

            case KeyEvent.KEYCODE_BACK:
            {

              //  onBackPressed();
                return true;
            }
        }
        return super.dispatchKeyEvent(event);
    }
*/

    public void StartUpScreenThread_dir_fill(Boolean full_read)
    {
        //  Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();



        Handler handler = new Handler(Looper.myLooper());


        runnable = () -> {

            if(zz_Config.TEST)
                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen A" );


            if(full_read){

                ((MyApplication) getApplication()).getVideo("aa_activity_home_screen");


                MyApplication.directories.clear();

                for (int i = 0; i < ((MyApplication) getApplication()).names.size(); i++)
                {

                    List<String> values = new ArrayList<String>();
                    values = ((MyApplication) getApplication()).map.get(((MyApplication) getApplication()).names.get(i));
                    //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
                    Directory directory = new Directory(((MyApplication) getApplication()).names.get(i), values.get(1) + " Videos");
                    MyApplication.directories.add(directory);
                }

            }

            if(zz_Config.TEST)
                Log.d("_#FILL_DATA", "StartUpScreenThread_dir_fill : aa_activity_home_screen B" );


           /*
            if(zz_Config.TEST)
            {
                Log.d("_#DIR_CONTENT_HASH", "Hash: "+ ((MyApplication) getApplication()).map.toString());
                Log.d("_#DIR_CONTENT_NAME", "Hash: "+ ((MyApplication) getApplication()).names.toString());

            }
*/
            handler.post(new Runnable() {
                @Override
                public void run() {
                  //  enable_tab();
                  //  viewPager.setCurrentItem(1);
                    // recyclerAdapter.notifyDataSetChanged();
                    ((MyApplication) getApplication()).recyclerAdapter.setRecyclerAdapter( MyApplication.directories,aa_activity_home_screen.this);
                    // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                    ((MyApplication) getApplication()).recyclerAdapter.notifyDataSetChanged();
                    ((MyApplication) getApplication()).mSwipeRefreshLayout.setRefreshing(false);


                    if(zz_Config.TEST)
                        Log.d("_#FILL_DATA", "Handler StartUpScreenThread_dir_fill : aa_activity_home_screen C" );

                }
            });


            //

        };
        readDirectoryThread =  new Thread(runnable);
        readDirectoryThread.start();


    }

    protected static String getAppTaskState(){



        StringBuilder stringBuilder=new StringBuilder();
        int totalNumberOfTasks=activityManager.getRunningTasks(10).size();//Returns total number of tasks - stacks
        stringBuilder.append("\nTotal Number of Tasks: "+totalNumberOfTasks+"\n\n");

        List<ActivityManager.RunningTaskInfo> taskInfo =activityManager.getRunningTasks(10);//returns List of RunningTaskInfo - corresponding to tasks - stacks

        for(ActivityManager.RunningTaskInfo info:taskInfo){
            stringBuilder.append("Task Id: "+info.id+", Number of Activities : "+info.numActivities+"\n");//Number of Activities in task - stack

            // Display the root Activity of task-stack
            stringBuilder.append("TopActivity: "+info.topActivity.getClassName().
                    toString().replace(name_file,"")+"\n");

            // Display the top Activity of task-stack
            stringBuilder.append("BaseActivity:"+info.baseActivity.getClassName().
                    toString().replace(name_file,"")+"\n");
            stringBuilder.append("\n\n");
        }
        return stringBuilder.toString();
    }


    @Override
    public void onBackPressed()
    {

        while (getFragmentManager().getBackStackEntryCount() > 0)
        {
            getFragmentManager().popBackStack(); // pop fragment here
        }

        zz_Config.IS_BACK=true;
        super.onBackPressed(); // after nothing is there default behavior of android works.


    }




/*
@Override
public void onBackPressed()
{
       // aa_activity_StartUp.aaStartUp = null;
        readDirectoryThread.interrupt();
        readDirectoryThread=null;
        runnable=null;

        zz_Config.IS_BACK=true;
        ((MyApplication) getApplication()).map.clear();
        ((MyApplication) getApplication()).map=null;
        ((MyApplication) getApplication()).names.clear();
        ((MyApplication) getApplication()).names=null;


        if(zz_Config.TEST){
            Log.d("_#_BACK_BUTTON", "onBackPressed:   : aa_activity_home_screen  ");
            // Log.d("_#_BACK_BUTTON",getAppTaskState());
        }

        super.onBackPressed();





    }

*/



}
