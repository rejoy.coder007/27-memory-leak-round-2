package com.example.rambo.zk_leakythread.aa_home_page.aa_tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.aa_spaceshare.SpaceShareFragment;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.FavFolderFragment;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.FolderFragment;
import com.example.rambo.zk_leakythread.za_global.MyApplication;
import com.example.rambo.zk_leakythread.zz_Config;

import java.util.ArrayList;

public class aa_home_screen_PagerAdapter extends FragmentPagerAdapter
{
    final int                                             PAGE_COUNT =3;
    private String tabTitles[] = new String[] { "SpaceShare", "Favorite", "FolderView" };
    public static final String ARG_PAGE = "ARG_PAGE";

   // private String tabTitles[] = new String[] { "Tab1", "Tab2" };
    private Context                                              context;

    public aa_home_screen_PagerAdapter(FragmentManager fm, Context context)
    {
        super(fm);
        this.context = context;

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "constructor:   : aa_activity_home_screen  ");

        }

    }

    @Override
    public int getCount()
    {
        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getCount:   : aa_activity_home_screen  _"+PAGE_COUNT);

        }

        return PAGE_COUNT;
    }

    @Override
    public Fragment getItem(int position)
    {
        Fragment fragment=null;

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getItem:   : aa_activity_home_screen  _ A_"+position+"_"+(position + 1));

        }

        switch (position){
            case 1: {
                fragment = new FavFolderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 1);

                fragment.setArguments(args);

                MyApplication.directories = new ArrayList<>();

                break;
            }


            case 0: {
                fragment = new SpaceShareFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 0);

                fragment.setArguments(args);



                break;
            }

            case 2:
            {
                fragment = new FolderFragment();
                Bundle args = new Bundle();
                args.putInt(ARG_PAGE, 2);
                fragment.setArguments(args);



                break;
            }



          //  default: fragment = ab_fragment_maker_homeScreen.newInstance(position + 1);

        }

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getItem:   : aa_activity_home_screen  _ B_"+position+"_"+(position + 1));

        }

        return fragment;

    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        // Generate title based on item position

        if(zz_Config.TEST)
        {
            Log.d("_#_FRAG_PAGER_ADAPTER", "getPageTitle:   : aa_activity_home_screen  _"+position);

        }

        return tabTitles[position];
    }
}
