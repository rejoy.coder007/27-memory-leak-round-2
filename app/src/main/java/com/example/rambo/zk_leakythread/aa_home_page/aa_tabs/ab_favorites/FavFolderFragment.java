package com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import com.example.rambo.zk_leakythread.R;
import com.example.rambo.zk_leakythread.aa_home_page.aa_activity_home_screen;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.Directory;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapter;
import com.example.rambo.zk_leakythread.za_global.MyApplication;
import com.example.rambo.zk_leakythread.zz_Config;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavFolderFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private   int mPage;

    public FavFolderFragment() {
        // Required empty public constructor
    }





    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_ON_CREATE", "onCreate fav fragment  : aa_activity_home_screen__ "+mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        View rootView=null;

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__ "+mPage);

        }


                rootView = inflater.inflate(R.layout.ad_b_fragment_home_fav, container, false);

     //   ((aa_activity_home_screen)getActivity()).back.setVisibility(View.VISIBLE);

                ((MyApplication) (getActivity()).getApplication()).handler = new Handler(getContext().getMainLooper());

                ((MyApplication) (getActivity()).getApplication()).recyclerView = (RecyclerView) rootView.findViewById(R.id.contact_recycleView);

                final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
                ((MyApplication) (getActivity()).getApplication()).recyclerView.setLayoutManager(linearLayoutManager);

                ((MyApplication) (getActivity()).getApplication()).recyclerView.setHasFixedSize(true);

                ((MyApplication) (getActivity()).getApplication()).recyclerAdapter=new RecyclerAdapter( MyApplication.directories, getContext());


                ((MyApplication) (getActivity()).getApplication()).recyclerView.setAdapter( ((MyApplication) (getActivity()).getApplication()).recyclerAdapter);


                // SwipeRefreshLayout
                ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
                ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,
                        android.R.color.holo_green_dark,
                        android.R.color.holo_orange_dark,
                        android.R.color.holo_blue_dark);

                StartThread_dir_update(false);


                ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // Your recyclerview reload logic function will be here!!!

                        if(zz_Config.TEST)
                            Log.d("_#_FAV_FRAG_REFRESH", "onRefresh fav fragment  : aa_activity_home_screen ");

                        // DirUpdateThreadFromFrag();

                        StartThread_dir_update(true);

                        ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout.setRefreshing(false);

                    }
                });




                //     ((aa_activity_home_screen) (getActivity())).viewPager.setCurrentItem(1);







        return rootView;
    }




    public void refresh(View view) {
        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen A__"+mPage);

        }

        StartThread_dir_update(true);

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_REFRESH", "refresh fav fragment    : aa_activity_home_screen B__  "+mPage);

        }


    }

    public void StartThread_dir_update(Boolean read_dir)
    {
        // Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable()
        {
            public void run()
            {

                if(zz_Config.TEST)
                {
                    if(read_dir)
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);
                    else
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);

                }

                if(read_dir)
                {
                    ((MyApplication) (getActivity()).getApplication()).getVideo("aa_activity_home_screen");
                }

                MyApplication.directories.clear();

                for (int i = 0; i < ((MyApplication) (getActivity()).getApplication()).names.size(); i++)
                {

                    List<String> values = new ArrayList<String>();
                    values = ((MyApplication) (getActivity()).getApplication()).map.get(((MyApplication) (getActivity()).getApplication()).names.get(i));
                    //  Log.d("DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
                    Directory directory = new Directory(((MyApplication) (getActivity()).getApplication()).names.get(i), values.get(1) + " Videos");
                    MyApplication.directories.add(directory);
                }


                try
                {
                    Thread.sleep(0);

                    ((MyApplication) (getActivity()).getApplication()).handler.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {


                            // recyclerAdapter.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter.setRecyclerAdapter( MyApplication.directories,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout.setRefreshing(false);

                            //   ((aa_activity_home_screen)getActivity()).viewPager.setCurrentItem(1);


                            if(zz_Config.TEST)
                            {

                                Log.d("_#_FAV_THREAD_HANDLER", "fav fagment handler for tab refresh from thread   : aa_activity_home_screen __  "+mPage);

                            }


                        }
                    });


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }



            }
        }).start();



    }

    // public


}
