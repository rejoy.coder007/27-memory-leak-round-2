package com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.rambo.zk_leakythread.R;


public class ViewHolder extends RecyclerView.ViewHolder {


    public TextView dir_name;
    public TextView video_count;
    public LinearLayout slice;


    public ViewHolder(@NonNull View itemView)
    {
        super(itemView);

        dir_name = itemView.findViewById(R.id.video_thumb);
        video_count = itemView.findViewById(R.id.file_name);
        slice  = itemView.findViewById(R.id.slice);
    }
}
