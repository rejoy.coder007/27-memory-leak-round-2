package com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.rambo.zk_leakythread.R;

import com.example.rambo.zk_leakythread.aa_home_page.aa_activity_home_screen;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.Directory;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapter;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.example.rambo.zk_leakythread.za_global.MyApplication;
import com.example.rambo.zk_leakythread.zz_Config;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FolderFragment extends Fragment {
    public static final String ARG_PAGE = "ARG_PAGE";
    private   int mPage;


    SwipeRefreshLayout mSwipeRefreshLayout;

    View rootView;
    RecyclerView recyclerView;
    Boolean mStopLoop = false;
    RecyclerAdapter_folder recyclerAdapterFolder;
    Handler handler;
    List<Directory_folder> directories = new ArrayList<>();
    public String dir;

    public FolderFragment() {
        // Required empty public constructor
    }






    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        mPage = getArguments().getInt(ARG_PAGE);

        if(zz_Config.TEST)
        {
                Log.d("_#_FOLD_FRAG_ON_CREATE", "FRAGMENT FOLDER ON_CREATE   : aa_activity_home_screen__ "+mPage);

        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {


        View rootView=null;

        if(zz_Config.TEST)
        {
            Log.d("_#_FAV_FRAG_ON_VIEW", " onCreateView fav fragment  : aa_activity_home_screen__ "+mPage);

        }

        MyApplication.directories_folder= new ArrayList<>();

        rootView = inflater.inflate(R.layout.ad_b_fragment_home_fav, container, false);

        //   ((aa_activity_home_screen)getActivity()).back.setVisibility(View.VISIBLE);

        ((MyApplication) (getActivity()).getApplication()).handler_folder = new Handler(getContext().getMainLooper());

        ((MyApplication) (getActivity()).getApplication()).recyclerView_folder = (RecyclerView) rootView.findViewById(R.id.contact_recycleView);

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        ((MyApplication) (getActivity()).getApplication()).recyclerView_folder.setLayoutManager(linearLayoutManager);

        ((MyApplication) (getActivity()).getApplication()).recyclerView_folder.setHasFixedSize(true);
       // MyApplication.directories_folder = new ArrayList<>();
        ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder=new RecyclerAdapter_folder( MyApplication.directories_folder, getContext());


        ((MyApplication) (getActivity()).getApplication()).recyclerView_folder.setAdapter( ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder);


        // SwipeRefreshLayout
        ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder = (SwipeRefreshLayout) rootView.findViewById(R.id.swipe_container);
        ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder.setColorSchemeResources(R.color.colorAccent,
                android.R.color.holo_green_dark,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_dark);

        StartThread_dir_update(false);


        ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Your recyclerview reload logic function will be here!!!

                if(zz_Config.TEST)
                    Log.d("_#_FAV_FRAG_REFRESH", "onRefresh fav fragment  : aa_activity_home_screen ");

                // DirUpdateThreadFromFrag();

               // StartThread_dir_update(true);

                ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder.setRefreshing(false);

            }
        });




        //     ((aa_activity_home_screen) (getActivity())).viewPager.setCurrentItem(1);




        return rootView;
    }




    public void StartThread_dir_update(Boolean read_dir)
    {
        // Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable()
        {
            public void run()
            {

                if(zz_Config.TEST)
                {
                    if(read_dir)
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);
                    else
                        Log.d("_#_FAV_THREAD_FULL_READ", "FAV FRAGMENT THREAD   : aa_activity_home_screen __  "+mPage);

                }

                if(read_dir)
                {
                   // ((MyApplication) (getActivity()).getApplication()).getVideo("aa_activity_home_screen");
                }


                MyApplication.directories_folder= new ArrayList<>();
                MyApplication.directories_folder.clear();

                for (int i = 0; i <((MyApplication) (getActivity()).getApplication()).names_folder.size(); i++)
                {

                     List<String> values = new ArrayList<String>();
                     values = ((MyApplication) (getActivity()).getApplication()).map_folder.get(((MyApplication) (getActivity()).getApplication()).names_folder.get(i));



                    Directory_folder directory = new Directory_folder(((MyApplication) (getActivity()).getApplication()).names_folder.get(i), values.get(0) + " Videos",values.get(1));


                     //Directory_folder directory = new Directory_folder( "hey",  "video","soso");
                     MyApplication.directories_folder.add(directory);
                }


                try
                {
                    Thread.sleep(0);

                    ((MyApplication) (getActivity()).getApplication()).handler_folder.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {


                            // recyclerAdapter.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder.setRecyclerAdapter( MyApplication.directories_folder,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            ((MyApplication) (getActivity()).getApplication()).recyclerAdapter_folder.notifyDataSetChanged();
                            ((MyApplication) (getActivity()).getApplication()).mSwipeRefreshLayout_folder.setRefreshing(false);

                              // ((aa_activity_home_screen)getActivity()).vi.setCurrentItem(1);


                            if(zz_Config.TEST)
                            {

                                Log.d("_#_FAV_THREAD_HANDLER", "fav fagment handler for tab refresh from thread   : aa_activity_home_screen __  "+mPage);

                            }


                        }
                    });


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }



            }
        }).start();



    }




}
