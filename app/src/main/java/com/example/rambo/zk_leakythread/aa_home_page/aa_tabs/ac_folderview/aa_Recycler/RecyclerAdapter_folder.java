package com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.example.rambo.zk_leakythread.R;
import com.example.rambo.zk_leakythread.zc_Glide.FutureStudioAppGlideModule;

import java.io.File;
import java.util.List;

public class RecyclerAdapter_folder extends RecyclerView.Adapter<ViewHolder_folder> {

    private List<Directory_folder> directories;
    private Context context;



    public RecyclerAdapter_folder(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;


    }

    public void setRecyclerAdapter(List<Directory_folder> directories, Context context)
    {
        this.directories = directories;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder_folder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i)
    {


        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.ag_folder_recycler_item, viewGroup, false);


        return new ViewHolder_folder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder_folder viewHolderFolder, final int i)
    {


        Directory_folder directory = this.directories.get(i);
        // viewHolder.video_thumb.setText(directory.video_thumb);

        String string_path = directory.file_name;
        String[] parts = string_path.split(".");
        // viewHolder.file_name.setText(parts[parts.length-1]);
        //viewHolderFolder.file_name.setText("nono");
        //viewHolderFolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));
        viewHolderFolder.file_name.setText( string_path.substring(0, string_path.lastIndexOf(".")));
        long interval = 1 * 200;
        //RequestOptions options = new RequestOptions().frame(interval);

        RequestOptions options = new RequestOptions();
        options.fitCenter();

        // int width = 200;
        // int height = 200;
        // LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width,height);
        //  viewHolder.video_thumb.setLayoutParams(parms);

        FutureStudioAppGlideModule futureStudioAppGlideModule = new FutureStudioAppGlideModule();



        futureStudioAppGlideModule.add_image_to_view(context,viewHolderFolder,Uri.fromFile( new File( directory.path_file ) ));


        // Directory_folder directoryFolder = this.directories.get(i);
      //  viewHolderFolder.file_name.setText(directoryFolder.file_name);


      //  List<String> values = new ArrayList<String>();
//        values = ((MyApplication) (MyApplication.getContext())).map.get(((MyApplication) (MyApplication.getContext())).names.get(i));








    }





    @Override
    public int getItemCount() {
        return directories.size();
    }
}
