package com.example.rambo.zk_leakythread.za_global;

import android.app.Application;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.Directory;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ab_favorites.aa_Recycler.RecyclerAdapter;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler.Directory_folder;
import com.example.rambo.zk_leakythread.aa_home_page.aa_tabs.ac_folderview.aa_Recycler.RecyclerAdapter_folder;
import com.example.rambo.zk_leakythread.zz_Config;
import com.squareup.leakcanary.LeakCanary;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyApplication extends Application
{
    public ViewPager viewPager=null;
    public Map<String, List<String>> map = new HashMap<String, List<String>>();
    public List<String> names = new ArrayList<String>();

    public Map<String, List<String>> map_folder = new HashMap<String, List<String>>();
    public List<String> names_folder = new ArrayList<String>();

    private static MyApplication mContext;


    public TabLayout tabLayout=null;


    public RecyclerView recyclerView;
    public Boolean mStopLoop = false;
    public RecyclerAdapter recyclerAdapter;
    public Handler handler;
    public static List<Directory> directories =null;
    public SwipeRefreshLayout mSwipeRefreshLayout;

    public RecyclerView recyclerView_folder;
    public Boolean mStopLoop_folder= false;
    public RecyclerAdapter_folder recyclerAdapter_folder;
    public Handler handler_folder;
    public static List<Directory_folder> directories_folder =null;
    public SwipeRefreshLayout mSwipeRefreshLayout_folder;


    @Override
    public void onCreate()
    {
        super.onCreate();
        mContext = this;

        if(zz_Config.TEST)
        {

            Log.d("_#_APPLICATION", "onCreate: MyApplication ");
        }


        if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);
        // Normal app init code...


    }

    public static MyApplication getContext() {
        return mContext;
    }

//////////////////////////////////////////////////////////////////////////////


    public void getfile(File dir)
    {



        File listFile[] = dir.listFiles();
        Boolean video_flag=false;
        Integer count=0;

        if (listFile != null && listFile.length > 0)
        {


            for (int i = 0; i < listFile.length; i++)
            {

                if (listFile[i].isDirectory())
                {
                    //fileList.add(listFile[i]);
                    getfile(listFile[i]);

                }
                else
                {


                    if (
                            listFile[i].getName().endsWith(".3gp")
                                    || listFile[i].getName().endsWith(".avi")
                                    || listFile[i].getName().endsWith(".flv")
                                    || listFile[i].getName().endsWith(".m4v")
                                    || listFile[i].getName().endsWith(".mkv")
                                    || listFile[i].getName().endsWith(".mov")

                                    || listFile[i].getName().endsWith(".mp4")
                                    || listFile[i].getName().endsWith(".mpeg")
                                    || listFile[i].getName().endsWith(".mpg")
                                    || listFile[i].getName().endsWith(".mts")
                                    || listFile[i].getName().endsWith(".vob")



                                    || listFile[i].getName().endsWith(".webm")
                                    || listFile[i].getName().endsWith(".wmv")


                            )

                    // if (listFile[i].getName().endsWith(".webm"))

                    {


                        video_flag=true;
                        count++;



                        // names.add(listFile[i].toString());
                        //  mAttachmentList.add(new AttachmentModel(listFile[i].getName()));
                    }
                }
            }

            if(video_flag)
            {
                String string_path = dir.toString();
                String[] parts = string_path.split("/");

                List<String> values = new ArrayList<String>();
                values.add(dir.toString());
                values.add(count.toString());

                names.add(parts[parts.length - 1]);
                // names.add("WMV");
                map.put(parts[parts.length - 1], values);


            }







        }
        //return fileList;
    }

    private void getInbox()
    {
        // holder.text.setTextColor(Color.RED);



        List<String> values_1 = new ArrayList<String>();
        values_1.add("Inbox.path");
        values_1.add("12");
        names.add("Inbox");
        map.put("Inbox", values_1);

        List<String> values_2 = new ArrayList<String>();
        values_2.add("Inbox.path");
        values_2.add("12");
        names.add("Private Folder");

        map.put("Private Folder", values_2);

    }

    public void getVideo(String activity_name)
    {
        names.clear();
        map.clear();
        File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath());

        if(zz_Config.TEST)
        {
            Log.d("_#GET_VID_ROOT_DIR", "Root Dir: "+dir.toString()+"_::_"+activity_name);
        }


        getInbox();


        List<String> values = new ArrayList<String>();







        getfile(dir );


    }


    public void getFilesFromDir(String file_root)
    {
        names_folder.clear();
        map_folder.clear();
        File dir = new File(file_root);


        File listFile[] = dir.listFiles();


        if (listFile != null && listFile.length > 0)
        {


            for (int i = 0; i < listFile.length; i++)
            {

                if (!listFile[i].isDirectory())
                {



                    if (
                            listFile[i].getName().endsWith(".3gp")
                                    || listFile[i].getName().endsWith(".avi")
                                    || listFile[i].getName().endsWith(".flv")
                                    || listFile[i].getName().endsWith(".m4v")
                                    || listFile[i].getName().endsWith(".mkv")
                                    || listFile[i].getName().endsWith(".mov")

                                    || listFile[i].getName().endsWith(".mp4")
                                    || listFile[i].getName().endsWith(".mpeg")
                                    || listFile[i].getName().endsWith(".mpg")
                                    || listFile[i].getName().endsWith(".mts")
                                    || listFile[i].getName().endsWith(".vob")



                                    || listFile[i].getName().endsWith(".webm")
                                    || listFile[i].getName().endsWith(".wmv")


                            )



                    {

                        //  Log.d("_DIR_PATH", "getFilesFromDir: "+listFile[i].getName());


                        List<String> values = new ArrayList<String>();
                        values.add(listFile[i].getName());
                        values.add(dir.toString()+"/"+listFile[i].getName());

                        names_folder.add(listFile[i].getName());
                        // na_foldermes.add("WMV");
                        map_folder.put(listFile[i].getName(), values);

                    }
                }
            }






        }





    }


    public void StartThread_files(final String file)
    {
        //Toast.makeText(getApplicationContext(), "Your Message", Toast.LENGTH_LONG).show();


        new Thread(new Runnable()
        {
            public void run()
            {


                getFilesFromDir(file);
                MyApplication.directories_folder.clear();

                for (int i = 0; i < names_folder.size(); i++)
                {

                    List<String> values = new ArrayList<String>();
                    values =map_folder.get(names_folder.get(i));
                    //Log.d("_DIR_N", "getfile: " + ((MyApplication) (getActivity()).getApplication()).names.get(i));
                    Directory_folder directory = new Directory_folder(    names_folder.get(i), values.get(0),values.get(1));
                    MyApplication.directories_folder.add(directory);

                }

                if(zz_Config.TEST){
                    Log.d("_##FOLDER", "run: "+map_folder.toString());
                    Log.d("_##FILES", "run: "+names_folder.toString());


                    for(int i=0;i<MyApplication.directories_folder.size();i++)
                    {
                        Log.d("_##video_thumb", "run: "+MyApplication.directories_folder.get(i).video_thumb);
                        Log.d("_##file_name", "run: "+MyApplication.directories_folder.get(i).file_name);
                        Log.d("_##path_file", "run: "+MyApplication.directories_folder.get(i).path_file);
                    }


                }



                try
                {
                    Thread.sleep(0);

                    handler_folder.post(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            // recyclerAdapter.notifyDataSetChanged();
                            recyclerAdapter_folder.setRecyclerAdapter(directories_folder,getContext());
                            // recyclerAdapter.notifyItemRangeChanged(0, directories.size());
                            recyclerAdapter_folder.notifyDataSetChanged();
                            mSwipeRefreshLayout_folder.setRefreshing(false);
                            viewPager.setCurrentItem(2);

                            //Log.d("#_DIR_TEST", "names: " + ((MyApplication) (getActivity()).getApplication()).names);
                           // Log.d("#_DIR_TEST", "map: " + ((MyApplication) (getActivity()).getApplication()).map);


                            //       enable_tab();

                        }
                    });


                }
                catch (InterruptedException e)
                {
                    e.printStackTrace();
                }
                Log.i("#INSIDE", " thread id: " + Thread.currentThread().getId());


            }
        }).start();


    }

}

